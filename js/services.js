(function () {
    "use strict"; //turn on javascript syntax mode

    angular.module("AlbumApp"). //call the angular module used in app.js
    service("dataService",         // the data service name
        ["$q",                     // dependency, $q handles promises, the request initially returns a promise, not the data
            "$http",                  // dependency, $http handles the ajax request
            function($q, $http) {     // the parameters must be in the same order as the dependencies
                var urlBase = "/portfolio/cm0665-assignment/server/index.php"; //gives url to be used to retrieve database info


                //function to retrieve albums
                this.getAlbum = function (searchTerm) {
                    var defer = $q.defer(),
                        data = { //contains parameters
                            action: "list",
                            subject: "albums",
                            searchterm: searchTerm
                        };
                    // make an ajax get call, chain calls to .success and .error which will resolve or reject the promise

                    $http.get(urlBase, {params: data, cache: true}).                          // notice the dot to start the chain to success()
                    success(function(response){
                        defer.resolve({
                            data: response.ResultSet.Result,         // create data property with value from response
                            rowCount: response.ResultSet.RowCount  // create rowCount property with value from response
                        });
                    }).
                    error(function(err){  //error function
                        defer.reject(err);
                    });
                    // the call to getAlbum, returns this promise which is fulfilled
                    return defer.promise;
                };

                //function to retrieve albums
                this.selectGenre = function () {
                    var defer = $q.defer(),
                        data = { //contains parameters
                            action: "list",
                            subject: "genres"
                        };
                    // make an ajax get call, chain calls to .success and .error which will resolve or reject the promise

                    $http.get(urlBase, {params: data, cache: true}).
                    success(function(response){
                        defer.resolve({
                            data: response.ResultSet.Result// create data property with value from response
                        });
                    }).
                    error(function(err){  //error function
                        defer.reject(err);
                    });
                    // the call to getAlbum, returns this promise which is fulfilled
                    return defer.promise;
                };


                //function to retrieve album tracks
                this.getTracks = function (album_id) {
                    var defer = $q.defer(),
                        data = {  //contains parameters
                            action: "list",
                            subject: "tracks",
                            id: album_id
                        };
                    $http.get(urlBase , {params: data, cache: false}).
                    success(function(response){
                        defer.resolve({
                            data: response.ResultSet.Result,
                            rowCount: response.ResultSet.RowCount
                        });
                    }).
                    error(function(err){  //error function
                        defer.reject(err);
                    });
                    return defer.promise;
                };
            }
        ]
    );
}());