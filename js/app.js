(function() {
    "use strict"; //turn on javascript syntax mode

    // Start new album application
    angular.module("AlbumApp",
        [
            "ngRoute"   // the only dependency at this stage, for routing
        ]
    ).  // chain the call to config
    config(
        [
            "$routeProvider",     // built in variable passed as a string
            function($routeProvider) {
                $routeProvider.
                when("/albums", {
                    //  templateUrl: "js/partials/album-list.html",
                    controller: "AlbumController" //name of controller handling the album info
                }).
                when("/:searchTerm", {
                    //  templateUrl: "js/partials/album-list.html",
                    controller: "AlbumController" //name of controller handling the album info
                }).
                when("/album/:album_id", {
                    templateUrl: "js/partials/track-list.html", //retrieves information from track-list.html when each album is clicked on
                    controller: "TracksController" //name of controller handling the track info
                }).
                otherwise({
                    redirectTo: "/"
                });
            }
        ]
    );  // end of config method
}());