(function () {
    "use strict"; //turn on javascript syntax mode

    angular
        .module("AlbumApp")  //module named in app.js
        .controller("IndexController", //index controller
            [
                "$scope",  //scope used to link to the view
                function ($scope) {
            //set messages for index page
                    $scope.title = "MVC Music";
                    $scope.subtitle = "Without music life would B flat";
                    $scope.message = "Click on album to view track list"
                }
            ]
        ).

    //controller for albums
    controller("AlbumController",
        [
            "$scope",
            "dataService",
            "$location",
            "$routeParams",
            function ($scope, dataService, $location, $routeParams) {
                $scope.searchterm = '';

                // search function from search form in index.html
                $scope.searchAlbums = function ($event, searchTerm) {
                    //check that the searchterm passed isnt empty (null)
                    if (searchTerm !== null) {
                        //change the url path to search/searchterm using the searchterm passed from search form (same as in app.js)
                        $location.path('/search/' + searchTerm);
                    }
                };

                //searching
            //make album function, pass in searchTerm if there is one
                var getAlbum = function () {
                    //pass into the dataservice to query database

                    dataService.getAlbum().then(  // then() is called when the promise is resolve or rejected
                        function(response){ //response holds data retrieved by dataService.getAlbum
                            $scope.albumCount  = response.rowCount + " album";
                            $scope.albums     = response.data;
                        },
                        function(err){ //function called if there is an error with dataService.getAlbum
                            $scope.status = "Unable to load data " + err;
                        },
                        function(notify){
                            console.log(notify);
                        }
                    ); // end of getAlbum().then

                };

                //declare empty object for selectedAlbum, to be used to pull tracks (getTracks)
                $scope.selectedalbum = {};
                $scope.selectAlbum = function ($event, album){ //define selectAlbum as a function with two parameters- event is the on-lick and album is the clicked on object
                    $scope.selectedAlbum = album;  //assign the album that is passed in second argument in function when clicked on
                    $location.path("/album/" + album.album_id); //set url to album/albumid
                };

                //searching
                $scope.searchAlbums = function ($event, searchTerm){ //define selectAlbum as a function with two parameters- event is the on-lick and album is the clicked on object
                    $location.path("/" + searchTerm); //set url to searchTerm
                };

                    getAlbum();


                //function for filtering by genre
                var selectGenre = function () {
                    dataService.selectGenre().then(  // then() is called when the promise is resolve or rejected
                        function(response){ //response holds data retrieved by dataService.getAlbum
                            $scope.genres     = response.data;
                        },
                        function(err){ //function called if there is an error with dataService.getAlbum
                            $scope.status = "Unable to load data " + err;
                        },
                        function(notify){
                            console.log(notify);
                        }
                    ); // end of selectGenre().
                };

                $scope.selectedGenre = {};
                $scope.selectGenre = function ($event, genre) { //define selectAlbum as a function with two parameters. event is the on-lick and album is the clicked on object
                    $scope.selectedGenre = genre;  //assign the album that is passed in second argument in function when clicked on
                };
                selectGenre();


            }
        ]
    ).
    //track controller
    controller("TracksController",
        [
            "$scope",
            "dataService",
            "$routeParams",
            function ($scope, dataService, $routeParams){
        //declare empty array
                $scope.tracks = [ ];
                $scope.trackCount = 0;
                //pass in album id to the get tracks function
                var getTracks = function (album_id) {
                    //use the dataservice to query the database
                    dataService.getTracks(album_id) .then(
                        function (response) { //response holds data retrieved by dataService.getTracks
                            $scope.trackCount = response.rowCount + " tracks";
                            $scope.tracks = response.data;
                        },
                        function (err){ //function called if there is an error with dataService.getTracks
                            $scope.status = "Unable to load data " + err;
                        }
                    );  // end of getTracks().then
                };

                // get tracks if albumid has been passed, don't bother otherwise
                if ($routeParams && $routeParams.album_id) {
                    getTracks($routeParams.album_id);
                }
            }
        ]
    )
}());