<?php
Class Session{
    private static $instance;

    function __construct(){
        session_start();
    }

    public static function getInstance() {
        if (!isset(self::$instance)) {
            self::$instance = new Session();
        }
        return self::$instance;
    }

    public function setProperty( $key, $val ) {
        $_SESSION[ $key ] = $val;
    }

    public function getProperty( $key ) {
        $returnValue = "";
        if (isset($_SESSION[$key])) {
            $returnValue = $_SESSION[$key];
        }
        return $returnValue;
    }
}