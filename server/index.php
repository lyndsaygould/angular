<?php
// Start the session
session_start();

//retrieve the database and recordset classes
require_once('../server/classes/pdoDB.class.php');
require_once('../server/classes/recordset.class.php');
require_once('../server/classes/session.class.php');

//get parameters passed in, set to null if none passed in (validation/error prevention)
$action  = isset($_REQUEST['action']) ? $_REQUEST['action'] : null;
$subject = isset($_REQUEST['subject']) ? $_REQUEST['subject'] : null;
$id      = isset($_REQUEST['id']) ? $_REQUEST['id'] : null;
$searchterm = isset($_REQUEST['searchTerm']) ? $_REQUEST['searchTerm'] : null;
$username = isset($_REQUEST['username']) ? $_REQUEST['username'] : null;
$password = isset($_REQUEST['password']) ? $_REQUEST['password'] : null;

// take from URL and concat action and subject with uppercase first letter of subject
$route = $action . ucfirst($subject); // eg list album becomes listAlbum (for cases)

$db = pdoDB::getConnection(); // connect to db

//set the header to JSON (to return JSON)
header("Content-Type: application/json");

// take the appropriate action based on the action and subject
switch ($route) {
    //new case for albums
    case 'listAlbums':
        //make condition if searchterm is entered
        $condition = '';
        if ($searchterm) {
            $condition = "WHERE albumName LIKE '%$searchterm%'
                          OR artistName LIKE '%$searchterm%'
                          OR genre LIKE '%$searchterm%'
                          GROUP BY i_album.album_id";
        }
        //query database to retrieve all album info
        $sqlAlbum = "SELECT i_album.artwork, i_album.album_id AS album_id, i_album.name AS albumName, i_album.year, i_artist.name AS artistName, i_track.total_time, i_genre.genre_id, i_genre.name AS genre
                     FROM i_album
                     JOIN i_album_track ON (i_album.album_id = i_album_track.album_id)
                     JOIN i_track ON (i_album_track.track_id = i_track.track_id)
                     JOIN i_artist ON (i_track.artist_id = i_artist.artist_id)
                     JOIN i_genre ON (i_album.genre_id = i_genre.genre_id)
                     ";
//add condition if applicable, to filtre results (condition)^
        if ($condition) {
            $sqlAlbum = $sqlAlbum . $condition;
        } else {
            $sqlAlbum = $sqlAlbum . "GROUP BY i_album.album_id ORDER BY i_album.name";
        }
        $rs        = new JSONRecordSet();
        $retrieval = $rs->getRecordSet($sqlAlbum);
        echo $retrieval;
        break;

    //new case for genres
    case 'listGenres':
        $condition = '';
        if (!empty($id)) {
            $id = $db->quote($id);
            $condition = "WHERE i_genre.genre_id = $id";
        }
        //sql statement to select genre information, used for filtering
        $sqlGenre = "SELECT DISTINCT genre_id, i_genre.name AS genre FROM i_genre $condition";
        $rs        = new JSONRecordSet();
        $retrieval = $rs->getRecordSet($sqlGenre);
        echo $retrieval;
        break;

    //new case for album tracks
    case 'listTracks':
        //get track id
        if (!empty($id)) {
            $id = $db->quote($id);
        }
        //filtre track SQL
        $sqlTracks = "
        SELECT  i_album_track.track_number, i_album.album_id, i_album.name AS albumName, i_track.name AS trackName, i_album_track.track_id, i_artist.name AS artistName,  i_track.play_count, i_track.size
        FROM i_album
        JOIN i_album_track ON (i_album.album_id = i_album_track.album_id)
        JOIN i_track ON (i_album_track.track_id = i_track.track_id)
        JOIN i_artist ON (i_track.artist_id = i_artist.artist_id)
        GROUP BY i_track.track_id HAVING i_album.album_id=$id
        ORDER BY i_album_track.track_number";

        $rs = new JSONRecordSet();
        $retrieval = $rs->getRecordSet($sqlTracks);
        echo $retrieval;

        break;

    case 'loginUser':
        if (!empty($username) && !empty($password)) { // if both username and password are present try and log in
            $rs       = new JSONRecordSet();  // this could be your XMLRecordSet if appropriate
            $session = Session::getInstance();

            // construct login sql using placeholders
            $loginSQL = "SELECT username, user_id, password FROM i_user WHERE user_id=:userid AND password=:password";
            // create an array for the placeholder values -notice I encrypt the password, in this example using md5
            $hash = password_hash($password, PASSWORD_DEFAULT);
            $params   = array(':userid' => $username, ':password' => password_verify($password, $hash));

            // use getRecordSet
            $user   = $rs->getRecordSet($loginSQL, 'ResultSet', $params);

            if ($user !== false) {  // store successful login details
             $session->setProperty('user', $user);
            }
            else { // if the log in failed unset any previously set value for user
                $session->removeKey('user');
                echo '{"status":{"error":"error", "text":"There is some error"}}';
            }
        }
        else { // either username or password wasn't present so remove any previously set user values
            $session->removeKey('user');
            echo '{"status":{"error":"error", "text":"Username and Password are required."}}';
        }
        break;

     // return a json encapsulated user object if logged in else a json error message
     // this could easily be changed for an xml object and message if required by client

    case 'isloggedinUser':
        $session = Session::getInstance();
        $user = $session->getProperty('user');
        echo($user);
        break;

     // remove the user session if logged in, return a json error message if not logged in

    case 'logoutUser':
        if ($session->removeKey('user')) {
            echo '{"status":{"text":"user logged out"}}';
        }
        else {
            echo '{"status":{"error":"error", "text":"no user logged in"}}';
        }
        break;

    default:
        echo '{"status":"error", "message":{"text": "default no action taken"}}';
        break;


}